import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
    loading: true,
    data: [],
}

export const getResponse = createAsyncThunk('education/getRequest', async (url) => {
    const response = await fetch(url);
    const data = await response.json()
    return data;
})

export const educationSlice = createSlice({
    name: 'education',
    initialState: initialState,

    extraReducers: (builder) => {
        // builder.addCase(getRequest.pending, (state) => {
        //     state.loading = true
        // })

        builder.addCase(getResponse.fulfilled, (state, action) => {
            state.data = action.payload.educations
            state.loading = false;
        })
        builder.addCase(getResponse.rejected, (action) => {
            console.log('error')
        })
    }
})


export default educationSlice.reducer;