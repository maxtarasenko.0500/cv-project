import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
    loading: true,
    data: [],
    toggle: false,
}

export const getResponse = createAsyncThunk('skills/getResponse', async (url) => {
    const response = await fetch(url);
    const data = await response.json()
    return data;
})

export const sendRequest = createAsyncThunk('skills/sendRequest', async (arg) => {
    const [url, data] = arg;
    const response = await fetch(url, {
        method: 'post',
        headers: {
            'Content-type': 'application/json',
            'Content-Security-Policy': 'script-src "*"'
        },
        body: JSON.stringify(data),
    })
    return response;
})

export const skillsSlice = createSlice({
    name: 'skills',
    initialState: initialState,
    reducers: {
        toggleForm: (state) => {
            state.toggle = !state.toggle;
        },

    },

    extraReducers: (builder) => {
        builder.addCase(getResponse.fulfilled, (state, action) => {
            // const test = action.payload.skills
            let result = []
            result.push(JSON.parse(localStorage.getItem("skills")).data)
            result.push(action.payload.skills)
            result = result.flat()
            state.data = result

            state.loading = false;
        })
        builder.addCase(getResponse.rejected, (action) => {
            console.log('error')
        })
    }
})

export const { toggleForm } = skillsSlice.actions


export default skillsSlice.reducer;