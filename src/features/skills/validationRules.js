import * as yup from 'yup';

export const basicSchema = yup.object().shape({
    skillName: yup
        .string()
        .required("Skill name is a required field"),
    skillRange: yup
        .number("Skill range must be a 'number' type")
        .required('Skill range is a required field')
        .min(10, 'Skill range must be greater than or equal to 10')
        .max(100, 'Skill range must be less than or equal to 100'),
})