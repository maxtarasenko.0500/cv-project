import React from "react";

export default function Info({ text = '' }) {
    return (
        <div>
            <p>{text}</p>
        </div>
    )
}