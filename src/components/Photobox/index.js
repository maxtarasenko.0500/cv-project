import React from "react";

export default function Photobox(props = {}) {
    const { name = '', title = '', description = '', avatar = '' } = props.props;

    if (name && title && description && avatar) {
        return (
            <div className="title-page-info">
                <figure className="title-page-info__photo-box">
                    <div className="crop-photo">
                        <img src={avatar} alt='Error' />

                    </div>
                    <figcaption>
                        <strong>{name}</strong>
                        {
                            <article>
                                <header className="title">
                                    {title}
                                </header>
                                <div className="description">
                                    {description}
                                </div>
                            </article>
                        }
                    </figcaption>
                </figure>
            </div>
        )
    } else if (!description || !title) {
        return <div className="navbar-info">
            <figure className="navbar-info__photo-box">
                <div className="crop-photo">
                    <img src={avatar} alt='Error' />

                </div>
                <figcaption>
                    <strong>{name}</strong>
                </figcaption>
            </figure>
        </div>
    } else if (!description || !title || !name) {
        return <div className="shrinked-navbar-info">
            <figure className="shrinked-info__photo-box">
                <div className="crop-photo">
                    <img src={avatar} alt='Error' />
                </div>
            </figure>
        </div>
    }
}