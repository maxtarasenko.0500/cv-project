import React, { useEffect, useState } from "react";
import { Provider, useSelector, useDispatch } from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";

import { getResponse } from "../../features/education/educationSlice";

export default function TimeLine() {
    const dispatch = useDispatch();
    const data = useSelector((state) => state.education.data)
    const loading = useSelector((state) => state.education.loading)
    const url = 'api/educations'

    useEffect(() => {
        dispatch(getResponse(url))
    }, [])

    const displayTimeline = () => {
        try {
            return <div className="timeline" disabled={loading}>
                <ul className='timeline-list'>
                    {data.map((element) => {
                        return (<li key={element.date}>
                            <div className="timeline-date">{element.date}</div>
                            <div className={['general-event', 'timeline-event']}>
                                <div className="info">
                                    <h3>{element.title}</h3>
                                    <p>{element.text}</p>
                                </div>
                            </div>
                        </li>)
                    })
                    }
                </ul>
                <div className="loading-status"><FontAwesomeIcon className="icon" icon={faSyncAlt} /></div>
            </div>
        } catch (Error) {
            console.log('e');
            return (<div className="timeline" disabled={loading}>
                <div className="error-banner">Something went wrong; please review your server connection!</div>
            </div>)

        }
    }


    return displayTimeline()
}