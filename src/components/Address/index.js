import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebook, faSkype } from '@fortawesome/free-brands-svg-icons'

export default function Address() {

    return (
        <address className="address">
            <dl>
                <dt><FontAwesomeIcon icon={faPhone} /></dt>
                <dd>
                    <strong><a href="tel:+500 342 242">500 342 242</a></strong>
                </dd>
            </dl>
            <dl>
                <dt><FontAwesomeIcon icon={faEnvelope} /></dt>
                <dd>
                    <strong><a href="mailto:office@kamsolutions.pl">office@kamsolutions.pl</a></strong>
                </dd>
            </dl>
            <dl>
                <dt><FontAwesomeIcon icon={faTwitter} /></dt>
                <dd>
                    <strong>Twitter</strong>
                    <br></br>
                    <a href="https://twitter.com/wordpress">https://twitter.com/wordpress</a>
                </dd>
            </dl>
            <dl>
                <dt><FontAwesomeIcon icon={faFacebook} /></dt>
                <dd>
                    <strong>Facebook</strong>
                    <br></br>
                    <a href="https://www.facebook.com/facebook">https://www.facebook.com/facebook</a>
                </dd>
            </dl>
            <dl>
                <dt><FontAwesomeIcon icon={faSkype} /></dt>
                <dd>
                    <strong>Skype</strong>
                    <br></br>
                    <a href="skype:kamsolutions.pl">kamsolutions.pl</a>
                </dd>
            </dl>
        </address>
    )
}