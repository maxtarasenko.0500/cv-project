import React, { useEffect, useState } from "react";
import { getResponse, sendRequest, toggleForm } from "../../features/skills/skillsSlice";
import { useDispatch, useSelector } from "../../app/store";

import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from "../Button";

import { Formik, Field, Form, ErrorMessage } from 'formik';
import { basicSchema } from "../../features/skills/validationRules";

export default function Skills() {
    const initialValues = {
        skillName: '',
        skillRange: '',
    }
    const dispatch = useDispatch()
    const data = useSelector((state) => state.skills.data)
    const loading = useSelector((state) => state.skills.loading)
    const [updateData, setUpdateData] = useState(0)
    const displayForm = useSelector((state) => state.skills.toggle)
    const url = '/api/skills'

    return (
        <React.Fragment>
            <div className="display-form"></div>
            <div className="add-skill" onClick={() => dispatch(toggleForm())}>
                <Button icon={<FontAwesomeIcon icon={faPenToSquare} />} text=' Open edit' />
            </div>
            <div className="skills-form" disabled={!displayForm}>
                <Formik initialValues={initialValues} validationSchema={basicSchema} onSubmit={async (values, { resetForm }) => {
                    // console.log(values)
                    dispatch(sendRequest([url, values]))
                    setUpdateData(updateData + 1);
                    dispatch(getResponse(url))

                    resetForm();
                }}>
                    {({ errors, touched, isValid }) => (
                        <Form>
                            <div className="skills-form__menu">
                                <label className='skills-form__menu-label' htmlFor="skillName">Skill Name:</label>
                                <Field className={`skills-form__menu-input ${errors.skillName && touched.skillName ? 'input-error' : ''}`} id='skillName' type='input' name='skillName' placeholder='Enter skill name' />
                            </div>
                            <ErrorMessage name="skillName" render={(msg) => <p className="error">{msg}</p>} />
                            <div className="skills-form__menu">
                                <label className='skills-form__menu-label' htmlFor="skillRange">Skill range:</label>
                                <Field className={`skills-form__menu-input ${errors.skillRange && touched.skillRange ? 'input-error' : ''}`} id='skillRange' type='input' name='skillRange' placeholder='Enter skill range' />
                            </div>
                            <ErrorMessage name="skillRange" render={(msg) => <p className="error">{msg}</p>} />
                            <div className="skills-form__menu_button" disabled={!isValid}>
                                <Button text="Add Skill" type="submit" />
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
            <ul className="skills-list">
                {!loading && data.map((element) => {
                    // <button type="submit">Submit</button>
                    return (
                        <li key={element.id} className={`skills ${(element.name).toLowerCase()}`} style={{ width: `${element.range}%` }}>
                            {element.name}
                        </li>
                    );
                })}
            </ul>
            <div className="scale-bar">
                <div className="scale-left"></div>
                <div className="scale-center"></div>
                <div className="scale-right"></div>

                <div className="beginner"><span>Beginner</span></div>
                <div className="proficient"><span>Proficient</span></div>
                <div className="expert"><span>Expert</span></div>
                <div className="master"><span>Master</span></div>
            </div>
        </React.Fragment>
    )
}