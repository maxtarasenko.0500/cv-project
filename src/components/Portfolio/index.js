import React, { useEffect, useState } from "react";
import Isotope from 'isotope-layout'

import uiLogo from '../../assets/images/ui.png';
import codeLogo from '../../assets/images/code.png';
import PortfolioInfo from "./PortfolioInfo";

export default function Portfolio() {
    const initialValues = {
        all: false,
        ui: false,
        code: false,
    }
    const [activate, setActivete] = useState({
        all: true,
        ui: false,
        code: false,
    })

    const isotope = React.useRef()
    const [filterKey, setFilterKey] = React.useState('*')

    useEffect(() => {
        isotope.current = new Isotope('.filter-container', {
            itemSelector: '.filter-item',
            layoutMode: 'fitRows',
        })

        return () => isotope.current.destroy();
    }, [])

    React.useEffect(() => {
        filterKey === '*'
            ? isotope.current.arrange({ filter: `*` })
            : isotope.current.arrange({ filter: `.${filterKey}` })
    }, [filterKey])


    const onFilter = key => (e) => {
        setActivete(() => ({
            ...initialValues,
            [e.target.innerHTML]: true,
        }));


        setFilterKey(key)
    }



    return (
        <div>
            <ul className="tabs">
                <ul>
                    <li className={activate.all ? 'active' : ''}><span onClick={onFilter('*')}>all</span></li>
                    <li className={activate.ui ? 'active' : ''}><span onClick={onFilter('ui')}>ui</span></li>
                    <li className={activate.code ? 'active' : ''}><span onClick={onFilter('code')}>code</span></li>
                </ul>
            </ul>
            <ul className="filter-container" >
                <li className="filter-item ui" >
                    <img src={uiLogo} alt="card" />
                    <PortfolioInfo title="Some text"
                        text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. 
                        In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis "
                        url="https://somesite.com"
                    />
                </li>
                <li className="filter-item ui" >
                    <img src={uiLogo} alt="card" />
                    <PortfolioInfo title="Some text"
                        text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. 
                        In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis "
                        url="https://somesite.com"
                    />
                </li>
                <li className="filter-item code" >
                    <img src={codeLogo} alt="card" />
                    <PortfolioInfo title="Some text"
                        text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. 
                        In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis "
                        url="https://somesite.com"
                    />
                </li>
                <li className="filter-item code" >
                    <img src={codeLogo} alt="card" />
                    <PortfolioInfo title="Some text"
                        text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. 
                        In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis "
                        url="https://somesite.com"
                    />
                </li>

            </ul>
        </div >
    )
}