import React from "react"

export default function PortfolioInfo({ title = '', text = '', url = '' }) {
    return (
        <div className="portfolio-info">
            <h2>{title}</h2>
            <p>{text}</p>
            <a href={url}>View source</a>
        </div>
    )
}