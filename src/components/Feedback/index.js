import React from "react";

export default function Feedback({ data = [] }) {

    return (<ul className="feedback">
        {
            data.map((element, index) => {
                return (<li key={index}>
                    <div className="info">
                        <p>{element.feedback}</p>
                    </div>
                    <div className="feedback-reporter">
                        <img className="feedback-reporter-photo" src={element.reporter.photoUrl.Logo} alt='Error' />
                        <cite>
                            {element.reporter.name},&#160;
                            <a href={element.reporter.citeUrl}>{element.reporter.citeUrl.replace(/httpsk?:\/\//i, '')}</a>
                        </cite>
                    </div>
                </li>)
            })
        }
    </ul>

    )
}