import React from "react";

export default function Expertise({ data = [] }) {
    return (<ul className="expertise-list">
        {
            data.map((element, index) => {
                return (<li key={index}>
                    <div className="expertise-list-date">
                        <h3>{element.info.company}</h3>
                        <div className="date">{element.date}</div>
                    </div>
                    <div className="expertise-list-info">
                        <h3>{element.info.job}</h3>
                        <p>{element.info.description}</p>
                    </div>
                </li>)
            })
        }
    </ul>)
}