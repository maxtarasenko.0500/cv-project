import React from "react";

export default function Button({ icon, text = '', type = 'button' }) {
    return (
        <div className="button-container">
            <button className='button' type={type}>
            <span className="symbol">{icon}</span>
            <span className="button-text">{text}</span>
        </button>
        </div >
    )
}   