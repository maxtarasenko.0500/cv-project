import React from "react";

export default function Box({ title = '', content = '' }) {
    return (
        <div className="info-box">
            <h2 className="info-box__title">{title}</h2>
            <div className="info-box__content">{content}</div>
        </div>
    )
}