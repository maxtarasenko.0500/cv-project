import React, { useState } from "react";
import { Link } from 'react-scroll'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faUser,
    faGraduationCap,
    faPen,
    faGem,
    faSuitcase,
    faLocationArrow,
    faComment

} from "@fortawesome/free-solid-svg-icons";

export default function Navigation() {
    const navTitles = [
        { icon: faUser, title: "About me", path: 'about' },
        { icon: faGraduationCap, title: "Education", path: 'education' },
        { icon: faPen, title: "Experience", path: 'experience' },
        { icon: faGem, title: "Skills", path: 'skills' },
        { icon: faSuitcase, title: "Portfolio", path: 'portfolio' },
        { icon: faLocationArrow, title: "Contacts", path: 'contacts' },
        { icon: faComment, title: "Feedbacks", path: 'feedbacks' },
    ]

    return (
        <div data-nav='list'>
            <ul className="navigation">
                {
                    navTitles.map((element) => {
                        return <li key={element.title}>
                            <Link activeClass="active" spy={true} smooth={true} className='navigation-link' duration={500} isDynamic={true} to={element.path}>
                                <dl>
                                    <dt>
                                        <FontAwesomeIcon icon={element.icon} /><span></span>
                                    </dt>
                                    <dd>
                                        <span>{element.title}</span>
                                    </dd>
                                </dl>
                            </Link>
                        </li>
                    })
                }
            </ul>
        </div >
    )
}