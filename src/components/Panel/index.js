import React, { useState } from "react";
import Photobox from '../Photobox/index'
import Navigation from '../Navigation/index'
import Button from '../Button/index'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faBars } from '@fortawesome/free-solid-svg-icons';

import { Link } from 'react-router-dom'


export default function Panel() {
    const photoboxProps = {
        name: "John Doe",
        avatar: "http://avatars0.githubusercontent.com/u/246180?v=4",
    }

    // const toggle = useRef(false)
    const [toggle, setToggle] = useState(false)

    const handleClick = () => {
        setToggle(!toggle)
    }

    return (
        <React.Fragment>
            <div className="panel">
                <div className={`sidebar ${toggle ? 'shrink' : ''}`}>
                    <Photobox props={photoboxProps} />
                    <Navigation />
                    <Link className="homelink" to='/'><Button icon={<FontAwesomeIcon icon={faChevronLeft} />} text='Go Back' /></Link>
                </div>
                <div className="hide-nav" onClick={handleClick}>
                    <Button icon={<FontAwesomeIcon icon={faBars} />} />
                </div>
            </div>
        </React.Fragment>
    )
}