import { configureStore, createListenerMiddleware, getDefaultMiddleware } from '@reduxjs/toolkit'
// import sectionManager from './sectionAction'
import educationSlice from '../features/education/educationSlice'
import skillsSlice, { sendRequest } from '../features/skills/skillsSlice'
import { useDispatch as _useDispatch, useSelector as _useSelector } from 'react-redux';

// Skills Listener Middleware
const listenerMiddleware = createListenerMiddleware();
listenerMiddleware.startListening({
    matcher: sendRequest,
    effect: (action, listenerAPI) => (
        localStorage.setItem(
            'skills',
            JSON.stringify((listenerAPI.getState()).skills)
        )
    )
})

// Configure store and getiing localStorage data
const skillsDataState = JSON.parse(localStorage.getItem("skills") || 'null');
export const store = configureStore({
    preloadedState: {
        skills: skillsDataState === null ? { data: [] } : skillsDataState,
    },
    reducer: {
        education: educationSlice,
        skills: skillsSlice
    },
    middleware: (getDefaultMiddleware) => [
        ...getDefaultMiddleware({
            serializableCheck: false
        }),
        listenerMiddleware.middleware
    ]
});

export const useDispatch = _useDispatch;
export const useSelector = _useSelector;


