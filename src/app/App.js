import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import HomePage from '../views/Home';
import InnerPage from '../views/Inner/index';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path='/' element={<HomePage />} />
          <Route path='/inner' element={<InnerPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
