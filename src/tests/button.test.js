import { userEvent, render, screen } from '@testing-library/react';
import Button from "../components/Button";
import { store } from "../app/store";
import { expect, jest, test, describe } from '@jest/globals';
import { Provider } from 'react-redux';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSyncAlt, faChevronLeft } from "@fortawesome/free-solid-svg-icons";


describe('Button Test', () => {
    test('Render Button Go Back', () => {
        render(<Provider store={store}><Button text='Go Back' /></Provider>);
        expect(screen.getByText('Go Back')).toBeInTheDocument()
    })

    test("Render Button Open edit", () => {
        render(<Provider store={store}><Button text='Open edit' /></Provider>);
        expect(screen.getByText('Open edit')).toBeInTheDocument()
    })

    describe('Render Buttons With symbols', () => {
        test("Render Button Chevron Left", () => {
            render(<Button icon={<FontAwesomeIcon icon={faChevronLeft} text='Go Back' />} text='Go Back' />)
            expect(screen.getByText('Go Back')).toBeInTheDocument()
        })
    })
})