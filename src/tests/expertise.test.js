import { userEvent, render, screen } from '@testing-library/react';
import { store } from "../app/store";
import { expect, jest, test, describe } from '@jest/globals';
import { Provider } from 'react-redux';
import React from 'react';

import Expertise from '../components/Expertise';

describe('Test Expertise', () => {
    test('Render Expertise', () => {
        render(<Provider store={store}><Expertise data={[{ date: '2013-2014', info: { company: 'Google', job: 'Front-end developer / php programmer', description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor' } }, { date: '2012', info: { company: 'Twitter', job: 'Web developer', description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor' } }]} /></Provider>)
        expect(screen.getByText('2013-2014')).toBeInTheDocument()
        expect(screen.getByText('Google')).toBeInTheDocument()
        expect(screen.getByText('Front-end developer / php programmer')).toBeInTheDocument()
    })
})