import { userEvent, render, screen } from '@testing-library/react';
import { store } from "../app/store";
import { expect, jest, test, describe } from '@jest/globals';
import { Provider } from 'react-redux';
import Feedback from '../components/Feedback';

describe('Feedback Test', () => {
    test('Render Feedback', () => {
        render(<Provider store={store}><Feedback data={[{ feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', reporter: { photoUrl: './user.jpg', name: 'John Doe', citeUrl: 'https://www.citeexample.com' } }, { feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', reporter: { photoUrl: './user.jpg', name: 'John Doe', citeUrl: 'https://www.citeexample.com' } }]} /></Provider>);
    })
})