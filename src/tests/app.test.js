import { userEvent, render, screen } from '@testing-library/react';
import Button from "../components/Button";
import { store } from "../app/store";
import { expect, jest, test, describe } from '@jest/globals';
import { Provider } from 'react-redux';

import App from '../app/App';

describe('Test App Module', () => {
    test('Render App', () => {
        render(<Provider store={store}><App /></Provider>)
    })
})