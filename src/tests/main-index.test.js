import { userEvent, render, screen } from '@testing-library/react';
import Button from "../components/Button";
import { store } from "../app/store";
import { expect, jest, test, describe } from '@jest/globals';
import { Provider } from 'react-redux';
import server from './/../services/server';
import React from 'react';

import App from '../app/App';

describe('Test Index Main', () => {
    test("Start Server", () => {
        server();
    })

    test('Render Index', () => {
        render(<Provider store={store}><App /></Provider>)
    })
})