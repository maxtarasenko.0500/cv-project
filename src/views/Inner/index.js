import React from 'react';

import Box from '../../components/Box/index'
import Expertise from '../../components/Expertise/index'
import Feedback from "../../components/Feedback";
import Logo from '../../../src/assets/images/user.png';
import Panel from '../../components/Panel';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Portfolio from '../../components/Portfolio';
import TimeLine from '../../components/Timeline/index';
import Address from '../../components/Address';
import Button from '../../components/Button';
import Skills from '../../components/Skills';

import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { animateScroll as scroll } from 'react-scroll';

export default function InnerPage() {

    const scrollToTop = () => {
        scroll.scrollToTop()
    }

    return (
        <React.Fragment>
            <nav>
                <Panel />
            </nav>
            <div className='content'>
                <div id='about' >
                    <Box title='About me' content='Lorem ipsum dolor sit amet, consectetuer adipiscing elit.' />
                </div>

                <div id='education'>
                    <Box title='Education' content={<TimeLine />} />
                </div>

                <div id='experience'>
                    <Box title='Experience' content={<Expertise data={[
                        {
                            date: '2013-2014',
                            info: {
                                company: 'Google',
                                job: 'Front-end developer / php programmer',
                                description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
                            }
                        },
                        {
                            date: '2012',
                            info: {
                                company: 'Twitter',
                                job: 'Web developer',
                                description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
                            }
                        }
                    ]} />} />
                </div>

                <div id='skills'>
                    <Box title='Skills' content={<Skills />} />
                </div>

                <div id='portfolio'>
                    <Box title='Portfolio' content={<Portfolio />} />
                </div>

                <div id='contacts'>
                    <Box title='Contacts' content={<Address />} />
                </div>

                <div id='feedbacks'>
                    <Box title='Feedback' content={<Feedback data={
                        [{
                            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                            reporter: {
                                photoUrl: { Logo },
                                name: 'John Doe',
                                citeUrl: 'https://www.citeexample.com'
                            }
                        },
                        {
                            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                            reporter: {
                                photoUrl: { Logo },
                                name: 'John Doe',
                                citeUrl: 'https://www.citeexample.com'
                            }
                        }]} />} />
                </div>
            </div>
            <div className='scroll-up' onClick={scrollToTop}>
                <Button icon={<FontAwesomeIcon icon={faChevronUp} />} />
            </div>
        </React.Fragment>
    )
}