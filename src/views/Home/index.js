import React from 'react-dom'
import Photobox from '../../components/Photobox/index'
import Button from '../../components/Button'
import { Link } from 'react-router-dom'

export default function HomePage() {
    const info = {
        name: 'John Doe',
        title: 'Programmer. Creative. Innovator',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque ',
        avatar: 'http://avatars0.githubusercontent.com/u/246180?v=4'
    }

    return (
            <div className='background'>
                <div className='content'>
                    <Photobox props={info} />
                    <Link to='/inner'><Button text='Know more' /></Link>
                </div>
            </div>
    )
}
