import { createServer, Model, Factory } from "miragejs";

export default function server() {
    createServer({
        models: {
            education: Model,
            skill: Model
        },

        factories: {
            education: Factory.extend({
                date() {
                    let min = 2000
                    let max = 2022

                    return Math.floor(Math.random() * (max - min + 1)) + min
                },

                title(i) {
                    return `Title ${i}`
                },

                text(i) {
                    i++;
                    if (i % 3 === 0) {
                        return "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n"

                    } else if (i % 2 === 0) {

                        return 'Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n'
                    } else {
                        return 'Elit voluptate ad nostrud laboris.Elit incididunt mollit enim enim id id laboris dolore et et mollit.Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi.Dolore eu fugiat consectetur nulla sunt Lorem ex ad.Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n'
                    }
                }
            }
            ),
            skill: Factory.extend({
                name() {
                    return 'Skill'
                },

                range() {
                    let min = 10
                    let max = 100

                    return Math.floor(Math.random() * (max - min + 1)) + min
                }
            })
        },

        seeds(server) {
            for (let i = 22; i >= 12; i--) {
                server.create('education', { date: `20${i}` });
            }
        },

        routes() {
            this.namespace = 'api';

            this.get('/educations', (schema) => {
                return schema.educations.all();
            }, { timing: 3000 })

            this.get('/skills', (schema, request) => {
                return schema.skills.all()
            }, { timing: 3000 })

            this.post('/skills', (schema, request) => {
                let attrs = JSON.parse(request.requestBody);
                console.log(attrs)
                return schema.skills.create({ name: attrs.skillName, range: attrs.skillRange })
            }, { timing: 3000 })
        }
    })
}

